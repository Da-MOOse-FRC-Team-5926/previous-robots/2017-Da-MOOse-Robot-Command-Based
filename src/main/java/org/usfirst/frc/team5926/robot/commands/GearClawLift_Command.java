package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/*
 * Gear Claw Lift command class
 */
public class GearClawLift_Command extends Command {
	
	public GearClawLift_Command() {
		requires(Robot.GearClawLift);
	}

	@Override
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		// Lower the Gear Claw Lift to the floor
		Robot.GearClawLift.clawLift(true);
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		
		// Raise the Gear Claw Lift into position
		Robot.GearClawLift.clawLift(false);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		end();
	}
}
