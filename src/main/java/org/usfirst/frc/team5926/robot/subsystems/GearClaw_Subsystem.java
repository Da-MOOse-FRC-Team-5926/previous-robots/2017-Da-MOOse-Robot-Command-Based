package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/* 
 * The GearClaw_Subsystem class contains all the to use the Gear Claw
 */

public class GearClaw_Subsystem extends Subsystem {
	
	// Declare the PCM Circuit for the Gear Claw
	private Solenoid clawSol = new Solenoid(RobotMap.PCM.clawCircuit);
	
	@Override
	protected void initDefaultCommand() {
	}
	
	// Gear Claw Function
	public void clawOpen(boolean open) {
		if (open == true) {
			
			// Open Gear Claw
			clawSol.set(true);
			
			// Send the state of the Gear Claw to the SmartDashboard
			SmartDashboard.putBoolean("claw", true);
		}
		else {
			
			// Close Gear Claw
			clawSol.set(false);
			
			// Send the state of the Gear Claw to the SmartDashboard
			SmartDashboard.putBoolean("claw", false);
		}
	}
		
}
