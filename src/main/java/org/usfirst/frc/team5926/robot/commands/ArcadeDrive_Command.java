package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 * Main class for the controlling the Drive Train during TeleOp.
 * A Logitech Extreme 3D Pro Joystick is used to control the movements of the robot on the field. 
 */

public class ArcadeDrive_Command extends Command {
	
	public ArcadeDrive_Command() {
		requires(Robot.DriveTrain);
	}
	
	@Override
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		// Set motion to the current Joystick position
		Robot.DriveTrain.teleopDrive(Robot.oi.getDriverJoystick());
		
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
	}

}
