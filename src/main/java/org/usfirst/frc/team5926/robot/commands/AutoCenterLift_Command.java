package org.usfirst.frc.team5926.robot.commands;

import org.usfirst.frc.team5926.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 * Auton Command for placing a Gear on the center lift peg.
 * Sadly we have no encoders so we just dead reckon...
 */

public class AutoCenterLift_Command extends Command {
	
	// Create time variables
	private long clawOpenTime;
	private long forwardTime;
	private long preClawTime;
	private long forwardStopTime;
	private long startTime;
    
	public AutoCenterLift_Command() {
		
		// Set time to action(s)
		preClawTime = 1000;
		forwardTime = 1700;
		
        requires(Robot.DriveTrain);
        requires(Robot.GearClaw);
        requires(Robot.FrameLight);
	}
	
	@Override
	protected void initialize() {
		
		// Get the current time when the command starts executing
		startTime = System.currentTimeMillis();
		
		// Calculate the time when action(s) will be executed
		forwardStopTime = startTime + forwardTime;
		clawOpenTime = forwardStopTime + preClawTime;
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		
		if (forwardStopTime >= System.currentTimeMillis()) {
			
			// Drive forward
			Robot.DriveTrain.autoDrive(-0.37, -0.39);
		}
		else {
			
			// Stop Driving
			Robot.DriveTrain.autoDrive(-0.0, -0.0);
		}
		
		if (System.currentTimeMillis() >= clawOpenTime) {
			
			// Open Gear Claw
			Robot.GearClaw.clawOpen(true);
			
			// Light up frame to signal Human Player to retrieve the gear
			Robot.FrameLight.frameGlow(true);
		}
		
		
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		
		// Close Gear Claw at end of Auto
		Robot.GearClaw.clawOpen(false);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		end();
	}

}
