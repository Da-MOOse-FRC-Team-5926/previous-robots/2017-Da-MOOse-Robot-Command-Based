package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Subsystem;

/* 
 * The RobotLift_Subsystem class contains all the to use the Rope Climber
 */

public class RobotLift_Subsystem extends Subsystem {
	
	// Declare the WPI_TalonSRX for the Rope Climber
	private WPI_TalonSRX robotLift = new WPI_TalonSRX(RobotMap.CAN.robotLift);
	
	@Override
	protected void initDefaultCommand() {
	}
	
	// High Speed Mode
	public void ropeClimberFast() {
		robotLift.set(-0.95);
	}
	
	// Low Speed Mode
	public void ropeClimberSlow() {
		robotLift.set(-0.65);
	}
	
	// Stop the Rope Climber
	public void ropeClimberOff() {
		robotLift.set(0.0);
	}
}
