
package org.usfirst.frc.team5926.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
// import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import org.usfirst.frc.team5926.robot.commands.AutoCenterLift_Command;
import org.usfirst.frc.team5926.robot.commands.AutoCrossBaseLine_Command;
import org.usfirst.frc.team5926.robot.commands.AutoRightLift_Command;
import org.usfirst.frc.team5926.robot.subsystems.DriveTrain_Subsystem;
import org.usfirst.frc.team5926.robot.subsystems.FrameLight_Subsystem;
import org.usfirst.frc.team5926.robot.subsystems.GearClawLift_Subsystem;
import org.usfirst.frc.team5926.robot.subsystems.GearClaw_Subsystem;
import org.usfirst.frc.team5926.robot.subsystems.RobotLift_Subsystem;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */

public class Robot extends TimedRobot {
	
	// Initialize all subsystems 
	public static final DriveTrain_Subsystem DriveTrain = new DriveTrain_Subsystem();
	public static final GearClaw_Subsystem GearClaw = new GearClaw_Subsystem();
	public static final GearClawLift_Subsystem GearClawLift = new GearClawLift_Subsystem();
	public static final RobotLift_Subsystem RobotLift = new RobotLift_Subsystem();
	public static final FrameLight_Subsystem FrameLight = new FrameLight_Subsystem();
	public static OI oi;

	Command autonomousCommand;
	SendableChooser<Command> chooser = new SendableChooser<>();

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	
	@Override
	public void robotInit() {
		oi = new OI();
		
		// Auto Modes

		chooser.setDefaultOption("Cross Base Line (Default)", new AutoCrossBaseLine_Command());
		chooser.addOption("Center Lift", new AutoCenterLift_Command());
		chooser.addOption("Right Lift", new AutoRightLift_Command());
		SmartDashboard.putData("Auto mode", chooser);
		
		// Setup Drive Train
		DriveTrain.driveInit();
		DriveTrain.driveBrakeMode(true);
		
		// Setup USB Webcam Streams
		CameraServer.getInstance().startAutomaticCapture(0).setFPS(18);
		CameraServer.getInstance().startAutomaticCapture(1).setFPS(18);
	}

	/**
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	
	@Override
	public void disabledInit() {
		
		// Lights off to prepare for Auton
		Robot.FrameLight.frameGlow(false);
	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard.
	 */
	
	@Override
	public void autonomousInit() {
		autonomousCommand = chooser.getSelected();

		// schedule the autonomous command
		if (autonomousCommand != null) {
			autonomousCommand.start();
		}
	}

	/**
	 * This function is called periodically during autonomous
	 */
	
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
	}

	@Override
	public void teleopInit() {
		
		/**
		 * This makes sure that the autonomous stops running when
		 * TeleOp starts running. If you want the autonomous to
		 * continue until interrupted by another command, remove
		 * this line or comment it out.
		 */
		
		if (autonomousCommand != null) {
			autonomousCommand.cancel();
		}
		
		// Enable the under glow for the rest of the match
		Robot.FrameLight.frameGlow(true);
	}

	/**
	 * This function is called periodically during operator control
	 */
	
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This function is called periodically during test mode
	 */
	
	@Override
	public void testPeriodic() {
	}
}
