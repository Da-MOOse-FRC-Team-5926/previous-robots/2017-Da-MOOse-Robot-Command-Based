package org.usfirst.frc.team5926.robot.subsystems;

import org.usfirst.frc.team5926.robot.RobotMap;
import org.usfirst.frc.team5926.robot.commands.ArcadeDrive_Command;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.drive.*;
import edu.wpi.first.wpilibj.command.Subsystem;

/* 
 * The DriveTrain_Subsystem class contains all the to use the Drive Train   
 */

public class DriveTrain_Subsystem extends Subsystem {
	
	// Declare the WPI_TalonSRX(s) for the Drive Train
	private WPI_TalonSRX frontLeft = new WPI_TalonSRX(RobotMap.CAN.frontLeftDrive),
			rearLeft = new WPI_TalonSRX(RobotMap.CAN.rearLeftDrive),
			frontRight = new WPI_TalonSRX(RobotMap.CAN.frontRightDrive),
			rearRight = new WPI_TalonSRX(RobotMap.CAN.rearRightDrive);
	
	// Create the Drive Train object
	private DifferentialDrive driveTrain = new DifferentialDrive(frontLeft, frontRight);
	
	@Override
	protected void initDefaultCommand() {
		
		// Set the default command for a subsystem here.
		setDefaultCommand(new ArcadeDrive_Command());
	}
	
	// Drive Motor Initialization function
	public void driveMotorInit(WPI_TalonSRX i) {
		driveMotorBrakeMode(i, false);
		//i.configPeakOutputVoltage(+12.0f, -12.0f);
		//i.configNominalOutputVoltage(+0.0f, -0.0f);
	}
	
	// Drive Train Initialization function
	public void driveInit() {
		driveMotorInit(frontLeft);
		driveMotorInit(rearLeft);
		driveMotorInit(frontRight);
		driveMotorInit(rearRight);
		rearLeft.follow(frontLeft);
		rearRight.follow(frontRight);
	}
		
	// Drive Motor Brake Mode
	public void driveMotorBrakeMode(WPI_TalonSRX i, boolean mode) {
		if(mode) {
			i.setNeutralMode(NeutralMode.Brake);
		}
		i.setNeutralMode(NeutralMode.Coast);
	}
	
	// Drive Train Brake Mode
	public void driveBrakeMode(boolean mode) {
		driveMotorBrakeMode(frontLeft, mode);
		driveMotorBrakeMode(rearLeft, mode);
		driveMotorBrakeMode(frontRight, mode);
		driveMotorBrakeMode(rearRight, mode);
	}
	
	// Default Drive Mode
	public void teleopDrive(Joystick driveStick){
		driveTrain.arcadeDrive(driveStick.getY()*0.95, -driveStick.getX()*0.75);
	}
	
	// Precision Drive Mode
	public void teleopDriveSlow(Joystick driveStick){
		driveTrain.arcadeDrive(driveStick.getY()*0.5, -driveStick.getX()*0.75);
	}
	
	// Auton Drive Mode 
	public void autoDrive(double L, double R){
		driveTrain.tankDrive(L, R, false);
	}
	
	public void stop() {
		driveTrain.arcadeDrive(0.0, 0.0);
	}
	
}
